
## Page 1

Pepper&Carrot
Bigtitle: Book 3: The Butterfly Effect


## Page 2


Copyright © David Revoy 2020.
www.peppercarrot.com
This work is licensed under a Creative Commons Attribution 4.0 International license.
The text of the license is available at https://creativecommons.org/licenses/by/4.0/.

Sources:
Artworks, translations and more can be found here: https://www.peppercarrot.com/static6/sources.

Attribution:

Hereva, the universe of Pepper&Carrot:
Creation: David Revoy.
Lead maintainer: Craig Maloney.
Writers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin.
Correctors: Alex Gryson, CGand, Hali, Moini, Willem Sonke.

Project management:
Technical maintenance and scripting: CalimeroTeknik, Craig Maloney , David Revoy, Gleki Arxokuna, GunChleoc, Jookia, Mjtalkiewicz (aka Player_2), Nicolas Artance, Noel, Quetzal2, Valvin, Xhire.
General maintenance of the database of SVGs: 3dnett, CalimeroTeknik, Châu, David Revoy, GunChleoc, Karl Ove Hufthammer, Marc "Mc" Jeanmougin, Midgard.
Website maintenance and new features: David Revoy, GunChleoc, Jookia, Valvin (on the top of a modified PluXML with plugin plxMyMultiLingue).

Book-publishing project specific:
Proofreading: Ángel, Arlo James Barnes, Craig Maloney, John Morahan, Martin Disch, Paul, Parnikkapore, Stephen Paul Weber, RW, Valvin.
Approved for free culture seal logo on the cover by Romaine.

Episode 22: The Voting System (published on 2017-05-30)
Art: David Revoy.   Scenario: David Revoy.
Beta-readers: Craig Maloney, Nicolas Artance, Valvin.
Translation [English]: Alex Gryson.
Proofreading: Craig Maloney.

Episode 23: Take a Chance (published on 2017-08-10)
Art: David Revoy.   Scenario: David Revoy.
Beta-readers: Alex Gryson, CalimeroTeknik, Craig Maloney, Nicolas Artance, Valvin.
Translation [English]: Alex Gryson.
Proofreading: CalimeroTeknik, Craig Maloney.

Episode 24: The Unity Tree (published on 2017-12-15)
Art: David Revoy.   Scenario: David Revoy.
Beta-readers: Alex Gryson, CalimeroTeknik, Craig Maloney, Jihoon Kim, Jookia, Nicolas Artance, Nikitin Stanislav, Valvin, xHire.
English (original version)
Proofreading: CalimeroTeknik, Craig Maloney, Jookia, Midgard.

Episode 25: There Are No Shortcuts (published on 2018-05-17)
Art: David Revoy.   Scenario: David Revoy.
Beta-readers: Craig Maloney, Jihoon Kim, Midgard, Nicolas Artance, Nikitin Stanislav, Valvin, xHire.
Translation [English]: Alex Gryson.

Episode 26: Books Are Great (published on 2018-07-28)
Art: David Revoy.   Scenario: David Revoy.
Script-doctor: Craig Maloney.
Beta-readers: Jihoon Kim, Jookia, Midgard, Nicolas Artance, Popolon, R J Quiralta, Valvin, xHire, Zeograd.
English (original version)
Proofreading: CalimeroTeknik, Craig Maloney.

Episode 27: Coriander's Invention (published on 2018-10-31)
Art: David Revoy.   Scenario: David Revoy.
Beta-readers: Amyspark, Bhoren, Butterfly, CalimeroTeknik, Craig Maloney, Denis Prisukhin, Gleki Arxokuna, Jihoon Kim, Jookia, Martin Disch, Midgard, Nicolas Artance, Nikitin Stanislav, Valvin, xHire.
English (original version)
Proofreading: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard.

Episode 28: The Festivities (published on 2019-01-24)
Art: David Revoy.   Scenario: David Revoy.
Beta-readers: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin.
English (original version)
Proofreading: CalimeroTeknik, Craig Maloney, Martin Disch.

Episode 29: Destroyer of Worlds (published on 2019-04-25)
Art: David Revoy.   Scenario: David Revoy.
Beta-readers: CalimeroTeknik, Craig Maloney, Martin Disch, Nicolas Artance, Valvin.
Translation [English]: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard.

Special thanks to:

All patrons of Pepper&Carrot! Your support made all of this possible.

All translators of Pepper&Carrot for their contributions:
Aldrian Obaja Muis, Alexandre Alapetite, Alexandre E. Almeida, Alex Filonenko, Alex Gryson, Alina The Hedgehog, Andrej Ficko, Andrew "Akari" Alexeyew, Antonio Parisi, Arild Torvund Olsen, Aslan Zamanloo, Binh Pham, Bonaventura Aditya Perdana, CalimeroTeknik, Carlo Gandolfi, colognella, Costin-Sorin Ionescu, Craig Maloney, David Revoy, Denis Prisukhin, Dika Michio, DikaMikaelMichio, Dimitris Axiotis, Emmiline Alapetite, Erwhann-Rouge / webmistusik, Erwhann-Rouge, Florin Sandru, František Veverka, Frederico Batista, George Karettas, Gilles-Philippe Morin, Giovanni Alfredo Garciliano Díaz, Gleki Arxokuna, grin, Grzegorz Kulik, Guillaume Lestringant, GunChleoc, guruguru, Gwenaëlle Lefeuvre, Gwenaëlle Lefeuvre's Dad, Halász Gábor "Hali", Helmar Suschka, Hongpyo Jang, Hulo Biral the Tomcat, Hồ Nhựt Châu, Ionicero, Jihoon Kim, Jorge Maldonado Ventura, Juanjo Faico, Juan José Segura, Julian Eric Ain, Julien R. Mauduit, Kaoseto, Kari Lehto, Karl Ove Hufthammer, karxusa, Kateřina Fleknová, Kim Jihoon, Konstantin Morenko, KuroDash, Leonid "Deburger" Androschuk, Lou Simons, Mahwiii, Marie Moestrup, Marina, Marko Ivancevic, Martin Disch, Martin Doucha, Midgard, Mikael Olofsson, Milena Petrovic, Mishvanda, Muhammad Nur Hidayat MNH48, Name, NayLedofich, Neha Deogade, Nicolas Artance, Nikitin Stanislav, Paolo Abes, Patrik Deriano, Peter Moonen, Philipp (hapit) Hemmer, Pranav Jerry, Quetzal2, Rafael Carreras, Ran Zhuang, Rebecca Breu, Reinaldo Calvin, Ret Samys, R J Quiralta, Scott Walters, Shikamaru "initbar" Yamamoto, sitelen, StarWish, StarWish, Stefan Carpentier, Steve Harris, Sölve Svartskogen, talime, Tharinda Divakara, Thomas Nordstrøm, Tirifto, Valvin, Willem Sonke, William Johnsson, Yan Jing, Zdenek Orsag.

The Framasoft team for hosting our oversized repositories via their Gitlab instance Framagit.

The Freenode staff for our #pepper&carrot IRC community channel.

All Free/Libre and open-source software because all Pepper&Carrot episodes are created using 100% Free/Libre software on a GNU/Linux operating system. The main ones used for production are:
- Krita for artworks (krita.org).
- Inkscape for vector and speechbubbles (inkscape.org).
- Blender for arwork and video editing (blender.org).
- Kdenlive for video editing (kdenlive.org).
- Scribus for the book project (scribus.net).
- Gmic for filters and effects (gmic.eu).
- Imagemagick & Bash for 90% of automation for the project.

And finally to all developers who interacted on fixing Pepper&Carrot specific bug-reports:
A.l.e (Scribus), Boudewijn Rempt (Krita) , David Tschumperlé (Gmic), Dmitry Kazakov (Krita) , Jean Ghali (Scribus), Marc Jeanmougin (Inkscape), Tavmjong Bah (Inkscape), Wolthera van Hövell tot Westerflier (Krita).

... and anyone I've missed.


## Page 63

To be continued...


## Page 64

Making-of Bonus
Inside episode 26

“Books are Great” is one of my favorite episodes of this book. I started the storyboard on paper (photo above) at the start of summer, 2018. I wrote down many ideas about the series during these days (you can see many storyboards on the table). But one of my favorite ideas was a special short adventure with Pepper and Carrot doing a speedrun in an abandoned castle.

After scanning the storyboard, I drew over these thumbnails with more advanced sketches (left and below) to catch the poses I had in mind. Then I started to clean the artworks (top of page, on the right). The backgrounds were designed in grayscale with simple shapes: I was afraid to lose myself in the details of the ruins of the old castle. On this page, you can also find a step-by-step drawing of the fountain and a concept art for the keeper of the castle.


## Page 66

Making-of Bonus
The Coronation of Coriander

Episodes 27, 28 and 29 (known as the Coronation of Coriander Arc) were probably the three most challenging episodes I ever had to draw. The three episodes have so many epic shots (screenshot above while painting the ceiling of the temple of Zombiah). The three episodes also feature many characters (below: the final scene of episode 27, with the crowd waiting to pass through the new security system).


## Page 67

During this long arc, I used 3D (Blender) to get a bit of assistance on the difficult perspective and objects (screenshot above). Once the camera was positioned the way I wanted, I just had to paint over the 3D blocks to get the result I had in mind. 

I also learned a lot about managing dialogue with multiple characters (on the right). I was very happy to write stories featuring Shichimi, Coriander, Saffron and Pepper... who knew Shichimi was so fond of good food?


## Page 68

The last episode of this arc (and also of this book) is Destroyer of Worlds. It's a long episode with many environments and complex camera moves, pushing the limits of my possibilities to the extreme within the constraints of Pepper&Carrot's short episode format.

I drew all of this episode on a computer using blue lines for backgrounds and black lines for characters (photo above while drawing the first panel). The rendering for this episode has a very anime feeling. That's probably because the background and characters were painted using different techniques: flat colors for the characters and painting for the backgrounds (step-by-step on the left).

Coloring this episode was also difficult: fireworks, monster from another dimension, fire --- I had to make a lot of tiny thumbnails for color research. I also had to prepare more designs than usual for the secondary characters appearing in this episode.

On the right, you can see my desk at this time with my cat Noutti sleeping (my inspiration for Carrot, of course). To design the expressions for the dialog, a sketchbook and a tiny mirror on my desk were my best friends.

That's all! Thanks for reading these short “making of” write-ups. If you liked them, you can find more detailed ones on my blog:
www.davidrevoy.com



## Page 71

Version 1
Desktop publishing done with Scribus 1.5.5 on Kubuntu Linux 20.04
Montauban, France - July, 2020
www.davidrevoy.com

www.peppercarrot.com
