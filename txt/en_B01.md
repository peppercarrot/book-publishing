
## Page 1

Pepper&Carrot
Bigtitle: Book 1: The Potion of Flight


## Page 2

Copyright © David Revoy 2020.
www.peppercarrot.com
This work is licensed under a Creative Commons Attribution 4.0 International license.
The text of the license is available at https://creativecommons.org/licenses/by/4.0/.

Sources:
Artworks, translations and more can be found here: https://www.peppercarrot.com/static6/sources.

Attribution:

Hereva, the universe of Pepper&Carrot:
Creation: David Revoy.
Lead maintainer: Craig Maloney.
Writers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin.
Correctors: Alex Gryson, CGand, Hali, Moini, Willem Sonke.

Project management:
Technical maintenance and scripting: CalimeroTeknik, Craig Maloney , David Revoy, Gleki Arxokuna, GunChleoc, Jookia, Mjtalkiewicz (aka Player_2), Nicolas Artance, Noel, Quetzal2, Valvin, Xhire.
General maintenance of the database of SVGs: 3dnett, CalimeroTeknik, Châu, David Revoy, GunChleoc, Karl Ove Hufthammer, Marc "Mc" Jeanmougin, Midgard.
Website maintenance and new features: David Revoy, GunChleoc, Jookia, Valvin (on the top of a modified PluXML with plugin plxMyMultiLingue).

Book-publishing project specific:
Proofreading: Ángel, Arlo James Barnes, Craig Maloney, John Morahan, Martin Disch, Paul, Parnikkapore, Stephen Paul Weber, RW, Valvin.
Approved for free culture seal logo on the cover by Romaine.

Episode 1: The Potion of Flight (published on 2014-05-10)
Art: David Revoy.   Scenario: David Revoy.
English (original version)
Proofreading: Alex Gryson, Amireeti.

Episode 2: Rainbow Potions (published on 2014-07-25)
Art: David Revoy.   Scenario: David Revoy.
English (original version)
Proofreading: Alex Gryson, Amireeti.

Episode 3: The Secret Ingredients (published on 2014-10-03)
Art: David Revoy.   Scenario: David Revoy.
English (original version)
Proofreading: Alex Gryson, Amireeti.

Episode 4: Stroke of Genius (published on 2014-11-21)
Art: David Revoy.   Scenario: David Revoy.
English (original version)
Proofreading: Alex Gryson, Amireeti, Luke Bertot, Rémi Rampin, Willem Sonke.

Episode 5: Special holiday episode (published on 2014-12-19)
Art: David Revoy.   Scenario: David Revoy.
English (original version)
Proofreading: Alex Gryson, Amireeti, Jihoon Kim.

Episode 6: The Potion Contest (published on 2015-03-28)
Art: David Revoy.   Scenario: David Revoy.
Translation [English]: Alex Gryson.

Episode 7: The Wish (published on 2015-04-30)
Art: David Revoy.   Scenario: David Revoy.
Translation [English]: Alex Gryson.
Proofreading: GunChleoc.

Episode 8: Pepper's Birthday Party (published on 2015-06-28)
Art: David Revoy.   Scenario: David Revoy.
Translation [English]: Alex Gryson.

Episode 9: The Remedy (published on 2015-07-31)
Art: David Revoy.   Scenario: David Revoy.
Translation [English]: Alex Gryson.

Episode 10: Summer Special (published on 2015-08-29)
Art: David Revoy.   Scenario: David Revoy.
Translation [English]: Alex Gryson.

Episode 11: The Witches of Chaosah (published on 2015-09-30)
Art: David Revoy.   Scenario: David Revoy.
Translation [English]: Alex Gryson.
Proofreading: Luke Bertot.

Special thanks to:

All patrons of Pepper&Carrot! Your support made all of this possible.

All translators of Pepper&Carrot for their contributions:
Aldrian Obaja Muis, Alexandre Alapetite, Alexandre E. Almeida, Alex Filonenko, Alex Gryson, Alina The Hedgehog, Andrej Ficko, Andrew "Akari" Alexeyew, Antonio Parisi, Arild Torvund Olsen, Aslan Zamanloo, Binh Pham, Bonaventura Aditya Perdana, CalimeroTeknik, Carlo Gandolfi, colognella, Costin-Sorin Ionescu, Craig Maloney, David Revoy, Denis Prisukhin, Dika Michio, DikaMikaelMichio, Dimitris Axiotis, Emmiline Alapetite, Erwhann-Rouge / webmistusik, Erwhann-Rouge, Florin Sandru, František Veverka, Frederico Batista, George Karettas, Gilles-Philippe Morin, Giovanni Alfredo Garciliano Díaz, Gleki Arxokuna, grin, Grzegorz Kulik, Guillaume Lestringant, GunChleoc, guruguru, Gwenaëlle Lefeuvre, Gwenaëlle Lefeuvre's Dad, Halász Gábor "Hali", Helmar Suschka, Hongpyo Jang, Hulo Biral the Tomcat, Hồ Nhựt Châu, Ionicero, Jihoon Kim, Jorge Maldonado Ventura, Juanjo Faico, Juan José Segura, Julian Eric Ain, Julien R. Mauduit, Kaoseto, Kari Lehto, Karl Ove Hufthammer, karxusa, Kateřina Fleknová, Kim Jihoon, Konstantin Morenko, KuroDash, Leonid "Deburger" Androschuk, Lou Simons, Mahwiii, Marie Moestrup, Marina, Marko Ivancevic, Martin Disch, Martin Doucha, Midgard, Mikael Olofsson, Milena Petrovic, Mishvanda, Muhammad Nur Hidayat MNH48, Name, NayLedofich, Neha Deogade, Nicolas Artance, Nikitin Stanislav, Paolo Abes, Patrik Deriano, Peter Moonen, Philipp (hapit) Hemmer, Pranav Jerry, Quetzal2, Rafael Carreras, Ran Zhuang, Rebecca Breu, Reinaldo Calvin, Ret Samys, R J Quiralta, Scott Walters, Shikamaru "initbar" Yamamoto, sitelen, StarWish, StarWish, Stefan Carpentier, Steve Harris, Sölve Svartskogen, talime, Tharinda Divakara, Thomas Nordstrøm, Tirifto, Valvin, Willem Sonke, William Johnsson, Yan Jing, Zdenek Orsag.

The Framasoft team for hosting our oversized repositories via their Gitlab instance Framagit.

The Freenode staff for our #pepper&carrot IRC community channel.

All Free/Libre and open-source software because all Pepper&Carrot episodes are created using 100% Free/Libre software on a GNU/Linux operating system. The main ones used for production are:
- Krita for artworks (krita.org).
- Inkscape for vector and speechbubbles (inkscape.org).
- Blender for arwork and video editing (blender.org).
- Kdenlive for video editing (kdenlive.org).
- Scribus for the book project (scribus.net).
- Gmic for filters and effects (gmic.eu).
- Imagemagick & Bash for 90% of automation for the project.

And finally to all developers who interacted on fixing Pepper&Carrot specific bug-reports:
A.l.e (Scribus), Boudewijn Rempt (Krita) , David Tschumperlé (Gmic), Dmitry Kazakov (Krita) , Jean Ghali (Scribus), Marc Jeanmougin (Inkscape), Tavmjong Bah (Inkscape), Wolthera van Hövell tot Westerflier (Krita).

... and anyone I've missed.


## Page 64

To be continued...


## Page 65

Sketchbook
Bonus


## Page 69

Signing session in Toulouse, France, in May 2017.

This book is a big deal for me in many ways: these are the first episodes of Pepper&Carrot, a project I started for fun on the Internet back in May, 2014 after a professional burn­out. I never imagined at the time I would one day spend years on a series read by millions of people across the world and being able to combine so many of my passions: drawing, painting, storytelling, cats, computers, GNU/Linux open­ source culture, Internet, old school RPG video­ games, old manga/anime, traditional art, Japanese culture and fantasy... Transforming the webcomic into a book made 100% with free/libre and open source software was also a technical challenge... It took me years of testing and research! I hope you like the result.

Also, I want to thank the contributors of Pepper&Carrot for making all this possible: patrons of the new episodes, technical contributors, translators, developers of the software I'm using... and you, the owner of this book.

The success you've given Pepper&Carrot is beyond anything I ever imagined.


My desk in 2014 and 2015, the place where I made all the episodes of this book.


## Page 71

Version 1
Desktop publishing done with Scribus 1.5.5 on Kubuntu Linux 20.04
Montauban, France - July, 2020
www.davidrevoy.com

www.peppercarrot.com
