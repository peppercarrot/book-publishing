# Pepper&Carrot Book-Publishing

This repository was created to host and manage the multilingual comic book
publishing of [Pepper&Carrot](https://www.peppercarrot.com).

## Install/Setup on 'buntu 20.04LTS:

### Libraries & Software required:

The main tools are: 

* Imagemagick
* Scribus 1.5.5 (!strict −if possible− no SVN/Git version: Scribus files are not backward compatible at all)
* Inkscape 1.0 (or greater)
* Krita 4.3 Appimage (or greater, but use appimage)
* Fontforge (Optional, to edit font and add missing characters)

The ```download.sh``` script uses a lot of libraries; here is a one liner install:

    sudo apt install scribus fontforge fontforge-extras pdftk pandoc git wget imagemagick diffutils rsync curl cryptsetup libgdk-pixbuf2.0-dev libxml2-utils libcurl4 unrar zip unzip jq

The main Ubuntu repository doesn't contain a new enough version of Inkscape, so the latest repo needs to be added: 

```sudo add-apt-repository ppa:inkscape.dev/stable```

After this, ```sudo apt install inkscape``` will install a compatible version.

### Fix Imagemagick memory limit.

By default, Imagemagick, a command line tool to manipulate raster images, is installed with a very low tolerance to large files. If we want to convert 300ppi PNG or JPG to CMYK this faulty default will return errors. To level up the possibilities of this fantastic library, edit this file as administrator (sudo):

    /etc/ImageMagick-6/policy.xml

Scroll the lines and upgrade the limit manually with the text editor. My setup use that:

    <policy domain="resource" name="memory" value="4GiB"/>
    <policy domain="resource" name="map" value="4GiB"/>
    <policy domain="resource" name="width" value="128KP"/>
    <policy domain="resource" name="height" value="128KP"/>
    <policy domain="resource" name="area" value="4GiB"/>
    <policy domain="resource" name="disk" value="6GiB"/>

also, allow PDF read/write (it is useful).  
Find the line:

    <policy domain="coder" rights="none" pattern="PDF" />

and change it to

    <policy domain="coder" rights="read | write" pattern="PDF" />

## Install/Setup using docker

If you use Docker, it can be installed by running:

    ./tools/docker.sh ../download.sh

And it will download and prepare all required files.

## Get the Color Profiles

The bookpublishing −If printed with drivethrucomics POD− will require two profiles, the CMYK one of the printer named CGATS21_CRPC1.icc and the one I use on Pepper&Carrot RGB source files: sRGB-elle-V2-srgbtrc.icc . You need to install this two profiles in this folder so Scribus and Krita can catch them.

For this simply run:

    ./tools/install-color-profiles.sh

Note: If the Scribus files complain about a missing PHIL or PHILIPS color profile; skip it: this is the color profile of my monitor for the rendering of colors on my workstation. Replace it with your own monitor profile, or if your monitor is not calibrated; use a basic sRGB profile (eg: 'sRGB Built in' or 'none').

## Get the fonts

The repository [peppercarrot/fonts](https://framagit.org/peppercarrot/fonts) contains all the fonts needed to edit the book-publishing repository and the webcomic in any language. To install (and later, also update) simply run:

    ./tools/install-fonts.sh

## Working on the books

Once everything is installed and setup, copy and paste the configuration template `config.sample` into `config` and edit it with your text editor. You'll be able to set the language here (the two letter ISO code of Pepper&Carrot and also adjust a debug level). Instructions will be written as comments starting by `#` in the file. 

When it's done, run `./download.sh`, this script will:

- Download all the SVGs translations of each pages (in `src/$lang/`).

- Download all PNGs comic panels (without speechbubble) (in `src/`).

- Download all extra PNGs artworks (artworks/publishing/vector, mostly for artbook) (in subdirectories on `src/`).

- Render a ready for printing CMYK tiff file with color transformation and sharpening (in `tiff/`). Scribus file will be linked to this tiff pictures only.

## Other

#### Mask

Masks are picture in black and white (in `src/_mask/`) that defines area that shouldn't be rendered (in black). The project use them to delete the footer at the end of episodes (eg. "To be continued") or details that could affect continuity of reading in a book (eg. Credit overlapping the last panel). The mask files are lightweight (247KB for 33 pages in high resolution), and they are hosted directly on this repository. 

#### How to edit the speechbubbles

If you want to edit the speechbubble; it is preferable to make the change on the translation project upstream ('webcomic' repository on Pepper&Carrot repo). The download.sh script will catch later the change and render only what is necessary. If you do a local editing of the SVG, they might be lost if you run the script again (unless you protect the file against writing).

#### Cache

If you run into issue of rendering (Error midway, corrupted files, etc) the ``cache`` folder might prevents re-rendering things later. You can delete ``cache`` directory and the ``./download.sh`` script will render all the pages and all the files again.

#### Txt

You'll find on the `txt` directory the extracted texts from the Scribus files that require translation (mainly for the artbook). There is no way to sync this text back into the Scribus if you translate them. You'll have to copy/paste the sentences one by one. But anyway, working on the translation with the text file is still more convenient than translating directly into Scribus; especially if you share it with other translators/proofreaders.

### Scribus File naming convention

    <Pepper&Carrot ISO lang>_B<book number>_<type(inside|cover)>_<format(hardcover|softcover)>_<Page count>_<printed size in inches>.sla

Notes: 'inside' files for softcover and hardcover are similar and produce similar PDFs.

### Exporting from Scribus

Once the Scribus editing is ready, you can export to PDF within Scribus. Unfortunately, Scribus will reset the path where you can export the file each time and you need to manually enter the directory "export" in the path to save the PDF on this directory. Also Scribus sometime doesn't remember the last exported settings, so it's always important to check them. Important option that sometime are changed:

- Tab General: PDF/X-3, Compression method: Automatic, Quality: Maximum, Max Img res: 300dpi

- Tab fonts: Click Embed all

- Tab prepress: check "Use document bleed", Infostring: but the same name as your filename.

A last advice: don't run multiple export in parallel or Scribus will crash.

### Sanify PDF for printer

Once the PDF is exported from Scribus; this one will not be accepted as it is by the POD service because of multiple bugs in Scribus concerning PDF-X.
You'll need to convert your exported PDF with Ghostscript.

If you exported your PDF into the 'export' folder; you'll find a script on it named ``./pdf-render.sh`` this script will take care of exporting everything:

- It will crop JPG for the cover on the shop (to create the product item) and put them back to RGB.

- It will create a Sample PDF of just the first pages, to propose it to read online as a sample.

- it will create the final RGB PDF you can sell as a digital download (glueing the cover+backcover and all the inside together in RGB, with a lightweight enough file size for tablet and e-reader).

- it will create \*_CMYK-sanified PDF files you can upload for the the "inside" that are usually rejected by the printer if coming directly from Scribus.


That's all :-)

## License

[Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
Read LICENSE files hosted in the same folder, a detailed attribution for the art and proofreading is inside each book (Scribus file). If you can't open them, check https://www.peppercarrot.com/en/static6/sources&page=credits

Important: Authors of all modifications, corrections or contributions to this repository
accepts to release their work under the same license. This do not apply if you fork this repository.

## Derivations and trademark issue:

If you publish a derivation of the book on your own, **please modify the publisher logo on the cover** of the book and name your **shop/publisher account** after your own name (or own company/publisher name) and describe on the product it is a derivation from the official and that you are not the artist. It's important to communicate clearly with the audience what your publishing of Pepper&Carrot is about. I'll not accept people impersonating me and/or try to make counterfeit of my work on the official release. Customising name, logo is the easiest way to avoid confusion. In case of doubt, contact me.
