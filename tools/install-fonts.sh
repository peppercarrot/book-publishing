#!/bin/bash
mkdir -p ~/.fonts
cd ~/.fonts
git clone https://framagit.org/peppercarrot/fonts.git peppercarrot-fonts
cd ~/.fonts/peppercarrot-fonts
git pull
