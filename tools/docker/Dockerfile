FROM ubuntu:20.10

# set some environment variables
ENV DEBIAN_FRONTEND noninteractive

# install dependencies
RUN apt-get update && apt-get install -y jq inkscape fontforge fontforge-extras texlive-full texlive-extra-utils pdftk pandoc git wget imagemagick diffutils rsync curl cryptsetup libgdk-pixbuf2.0-dev libxml2-utils libcurl4 unrar zip unzip parallel libnotify-bin

# setup imagemagick
COPY etc/ImageMagick-6/policy.xml /etc/ImageMagick-6/policy.xml

# install fonts
WORKDIR /usr/share/fonts/
RUN git clone https://framagit.org/peppercarrot/fonts.git peppercarrot-fonts
# refresh system font cache
RUN fc-cache -f -v

# create peppercarrot user
RUN useradd -m peppercarrot
USER peppercarrot

# install color profiles
RUN mkdir -p /home/peppercarrot/.local/share/color/icc
WORKDIR /home/peppercarrot/.local/share/color/icc
RUN wget -N -q https://www.peppercarrot.com/0_sources/0ther/book-publishing/icc/CGATS21_CRPC1.icc
RUN wget -N -q https://www.peppercarrot.com/0_sources/0ther/book-publishing/icc/sRGB-elle-V2-srgbtrc.icc

WORKDIR /home/peppercarrot/book-publishing
