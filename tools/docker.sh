docker build ./docker -t peppercarrot-book-publishing:latest && \
docker run --rm -it -e DEBUG="${DEBUG}" -e LANG="${LANG}" -e TEMP_PATH="${TEMP_PATH}" -v "${PWD}:/home/peppercarrot/book-publishing:rw" peppercarrot-book-publishing bash $1
